package main.java.com.isep;

import com.isep.Case;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static com.isep.Game.displayBoard;
import static com.isep.Plaque.creatPlaque;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PlaqueTest {
    @Test
    public void creatPlaqueTest(){
        List<Case> recupfichier = new ArrayList<>();
        List<Case> test = new ArrayList<>();
        test.add(new Case(false,false,false,false,0,0,"A"));
        test.add(new Case(false,false,false,true,0,0,"B"));
        test.add(new Case(false,false,true,true,0,0,"C"));
        test.add(new Case(false,true,true,true,0,0,"D"));
        test.add(new Case(true,true,true,true,0,0,"E"));

        recupfichier = creatPlaque("test/main/resources/board/plaquetest1");
        //assertEquals(test,recupfichier);
    }
}
