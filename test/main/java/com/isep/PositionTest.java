package main.java.com.isep;

import com.isep.Piece;
import com.isep.Position;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PositionTest {

    @Test
    public void testGetColor(){
        Piece piece= new Piece(1, new Position(0,1), new Position(2,3));
        assertEquals(1,piece.getColor());
    }

    @Test
    public void testGetRow(){
        Position position= new Position(1,0);
        assertEquals(1,position.getRow());
    }

    @Test
    public void testGetCol(){
        Position position= new Position(1,0);
        assertEquals(0,position.getCol());
    }

    @Test
    public void testSetRow(){
        Position position= new Position(1,0);
        position.setRow(3);
        assertEquals(3,position.getRow());
    }

    @Test
    public void testSetCol(){
        Position position= new Position(1,0);
        position.setCol(2);
        assertEquals(2,position.getCol());
    }

}
