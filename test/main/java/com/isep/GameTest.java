package main.java.com.isep;

import com.isep.Case;
import com.isep.Game;
import com.isep.Piece;
import com.isep.Position;
import com.utils.InputParser;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static com.isep.Game.*;
import static org.junit.jupiter.api.Assertions.*;

public class GameTest {
    //public static List<Piece> pieceList = new ArrayList<>();


    @Test
    void synchroPieceBoardTest(){
        Case caseTest = new Case(true,true,true,true,0,0,"A");
        Case[][] board = {{caseTest}};

        Case caseTestCompare = new Case(true,true,true,true,1,1,"A");
        Case[][] boardCompare = {{caseTestCompare}};


                pieceList.add(new Piece(1,new Position(0,0),new Position(0,0)));
        board = synchroPieceBoard(board);

        assertEquals(board[0][0].getPiece(),boardCompare[0][0].getPiece());
    }


    @Test
    void choiceOptionTestFalse() throws InterruptedException {
        System.setIn(new ByteArrayInputStream("0\n".getBytes()));
        choiceOption();
        assertFalse(option);
    }

    @Test
    void choiceOptionTestTrue() throws InterruptedException {
        System.setIn(new ByteArrayInputStream("1\n".getBytes()));
        choiceOption();
        assertTrue(option);
        System.setIn(System.in);

    }


    @Test
    void rulesTest() throws InterruptedException {
        System.setIn(new ByteArrayInputStream("1\n".getBytes()));
        rules();
        assertTrue(true);
        System.setIn(System.in);

    }
}
