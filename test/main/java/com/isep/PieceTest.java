package main.java.com.isep;

import com.isep.Piece;
import com.isep.Position;
import javafx.geometry.Pos;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PieceTest {
    Position position = new Position(1,1);
    Piece piece = new Piece(1,position,position);
    @Test
    void setPositionInitialTest(){
        Position newPosition = new Position(0,0) ;
        piece.setPositionInitial(newPosition);
        assertEquals(piece.getPositionInitial(),newPosition);
    }
}
