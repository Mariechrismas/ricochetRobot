package main.java.com.isep;

import com.isep.Player;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PlayerTest {

    @Test
    public void testGetName(){
        Player player = new Player("Joueur", 0);
        assertEquals("Joueur", player.getName());
    }

    @Test
    public void testGetScore(){
        Player player = new Player("Joueur", 0);
        assertEquals(0, player.getScore());
    }

    @Test
    public void testSetName(){
        Player player = new Player("Joueur", 0);
        player.setName("Perdant");
        assertEquals("Perdant", player.getName());
    }

    @Test
    public void testSetScore(){
        Player player = new Player("Joueur", 0);
        player.setScore(1000);
        assertEquals(1000, player.getScore());
    }

}
