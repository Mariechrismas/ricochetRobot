package main.java.com.utils;

import com.utils.InputParser;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class InputParserTest {

    @Test
    public void testTimerNull() {
        String value = InputParser.timer(System.currentTimeMillis(), 3);
        assertNull(value);
    }

    @Test
    public void testTimerTimeOut() throws InterruptedException {
        String value;
        int j=1, compteur=0;
        long i= System.currentTimeMillis();
        do {
            value= InputParser.timer(i, j);
            compteur++;
        }while (compteur>=j);
        assertEquals("Time out", value);
    }
}
