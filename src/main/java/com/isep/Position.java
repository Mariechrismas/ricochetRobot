package com.isep;

public class Position {
    int row;
    int col;

    /**
     *  Position(int positionRow, int positionCol), fonction permettant de définir la position d'un pion (colonne,
     *  ligne) à partir de fonctions préexistantes
     */

    public Position(int positionRow, int positionCol){
        this.row=positionRow;
        this.col=positionCol;
    }

    /**
     *  getRow(), fonction permettant de récupérer le numéro de ligne
     */

    public int getRow() {
        return row;
    }

    /**
     *  getCol(), fonction permettant de récupérer le numéro de colonne
     */

    public int getCol() {
        return col;
    }

    /**
     *  setRow(int row), fonction permettant d'établir le numéro de colonne
     */

    public void setRow(int row) {
        this.row = row;
    }

    /**
     *  setCol(int col), fonction permettant d'établir le numéro de ligne
     */

    public void setCol(int col) {
        this.col = col;
    }
}
