package com.isep;

import com.utils.InputParser;

import static com.utils.InputParser.writeLn;

public class Move {

    /**
     * move(Case [][] board, Piece piece), fonction permettant de déplacer les pions (en haut, en bas, à gauche,
     * à droite) à partir de fonctions prédéfinies
     */

    public static Piece move(Case [][] board, Piece piece) throws InterruptedException {
        writeLn("Dans quel direction voulez vous le déplacer:");
        writeLn("1: haut\n2: Bas\n3: Gauche\n4: Droite");
        int choiceMove = InputParser.scannerInt();
        Piece pieceReturn = null;
        switch (choiceMove) {
            case 1:
                pieceReturn = getUpper(board, piece);
                break;
            case 2:
                pieceReturn = getLower(board, piece);
                break;
            case 3:
                pieceReturn = getLeft(board, piece);
                break;
            case 4:
                pieceReturn = getRight(board, piece);
                break;
            default:
                break;
        }
        return pieceReturn;
    }

    /**
     *  getUpper(Case[][] board,Piece piece), fonction permettant de déplacer le pion vers le haut
     */

    public static Piece getUpper(Case[][] board,Piece piece) {
        int col= piece.getPosition().getCol();
        int row = piece.getPosition().getRow();

        if (piece.getColor()!=5){
            while (!board[col][row].wallNorth && (board[col-1][row].piece == piece.getColor() || board[col-1][row].piece == 0)){
                col--;
            }
        }else{
            while (col!=0&&(board[col-1][row].piece == piece.getColor() || board[col-1][row].piece == 0)){
                col--;
            }
        }
        piece.getPosition().setCol(col);
        return piece;
    }

    /**
     *  getLower(Case[][] board,Piece piece), fonction permettant de déplacer le pion vers le bas
     */

    public static Piece getLower(Case[][] board,Piece piece) {
        int col= piece.getPosition().getCol();
        int row = piece.getPosition().getRow();
        if (piece.getColor()!=5) {
            while (!board[col][row].wallSouth && (board[col + 1][row].piece == piece.getColor() || board[col + 1][row].piece == 0)) {
                col++;
            }
        }else{
            while (col!=15&& (board[col + 1][row].piece == piece.getColor() || board[col + 1][row].piece == 0)) {
                col++;
            }
        }
        piece.getPosition().setCol(col);
        return piece;

    }

    /**
     *  getRight(Case[][] board,Piece piece), fonction permettant de déplacer le pion vers la droite
     */

    public static Piece getRight(Case[][] board,Piece piece) {
        int col= piece.getPosition().getCol();
        int row = piece.getPosition().getRow();
        if (piece.getColor()!=5) {
            while (!board[col][row].wallEast &&(board[col][row+1].piece == piece.getColor() || board[col][row+1].piece == 0 )){
                row++;
            }
        }else {
            while (row!=15 &&(board[col][row+1].piece == piece.getColor() || board[col][row+1].piece == 0 )){
                row++;
            }
        }

        piece.getPosition().setRow(row);
        return piece;
    }

    /**
     *  getLeft(Case[][] board,Piece piece), fonction permettant de déplacer le pion vers la gauche
     */

    public static Piece getLeft(Case[][] board,Piece piece) {
        int col= piece.getPosition().getCol();
        int row = piece.getPosition().getRow();
        if (piece.getColor()!=5) {
            while (!board[col][row].wallWest && (board[col][row - 1].piece == piece.getColor() || board[col][row - 1].piece == 0)) {
                row--;
            }
        }else  {
            while (row!=0 && (board[col][row - 1].piece == piece.getColor() || board[col][row - 1].piece == 0)) {
                row--;
            }
        }
        piece.getPosition().setRow(row);
        return piece;
    }

}
