package com.isep;

public class Case {
        boolean wallNorth;
        boolean wallSouth;
        boolean wallWest;
        boolean wallEast;
        int piece;
        int pieceJeton;
        String objectif;

        /**
         * Case(boolean wallNorth,boolean wallSouth,boolean wallEast,boolean wallWest, int piece,int pieceJeton,String
         * objectif ),fonction permettant de définir les murs (Nord, Sud, Ouest, Est), si c'est l'emplacement d'un pion,
         * si c'est l'emplacement de départ d'un pion, et enfin si c'est l'emplacement d'un objectif
         */

        public Case(boolean wallNorth,boolean wallSouth,boolean wallEast,boolean wallWest, int piece,int pieceJeton,String objectif ){
                this.wallNorth=wallNorth;
                this.wallSouth=wallSouth;
                this.wallWest=wallWest;
                this.wallEast=wallEast;
                this.piece=piece;
                this.pieceJeton=pieceJeton;
                this.objectif=objectif;

        }


        public int getPiece() {
                return piece;
        }

}

