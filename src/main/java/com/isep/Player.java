package com.isep;

public class Player {

    protected String name;
    protected int score;

    /**
     *  Player(String playerName, int playerScore), fonction permettant de définir les paramètres du joueur/des
     *  joueurs (nom et score) à partir de fonctions préexistantes
     */

    public Player(String playerName, int playerScore){
        this.name=playerName;
        this.score=playerScore;
    }

    /**
     *  getName(), fonction permettant de récupérer le nom du joueur
     */

    public String getName() {
        return name;
    }

    /**
     *  getScore(), fonction permettant de récupérer le score du joueur
     */

    public int getScore() {
        return score;
    }

    /**
     *  setName(String name), fonction permettant d'établir le nom du joueur
     */

    public void setName(String name) {
        this.name = name;
    }

    /**
     *  setScore(int score), fonction permettant d'établir le score du joueur
     */

    public void setScore(int score) {
        this.score = score;
    }
}
