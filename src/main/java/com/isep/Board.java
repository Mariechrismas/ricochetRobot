package com.isep;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicReference;

import static com.isep.Plaque.creatPlaque;
import static com.utils.InputParser.readFile;


public class Board {
    public static List<String> listePlaquePng = new ArrayList<>();

    /**
     * creatBoard (), fonction permettant de créer le plateau de jeu en choississant aléatoirement entre les 2
     * plateaux de chaque couleur, le compteur fusionne après le 4 plaques tirées
     * PS : il y a une faute on sait, mais on trouve ça drôle de le rappeler à qq
     */

    public static Case[][] creatBoard (){
        List<Case> yellowPlaque = new ArrayList<>();
        List<Case> greenPlaque = new ArrayList<>();
        List<Case> bluePlaque = new ArrayList<>();
        List<Case> redPlaque = new ArrayList<>();
        int lengthBoard = 16;
        int widthBoard = 16;
        Case[][] board = new Case[lengthBoard][widthBoard];

        int length = lengthBoard/2;
        int width = widthBoard/2;

        int nombrePlaque = 2;
        ArrayList<String> listePlaque = readFile("src/main/resources/boardText/listePlaque");
        Random random = new Random();
        int choiceY = random.nextInt(2);
        int choiceG = random.nextInt(2)+ nombrePlaque;
        int choiceB = random.nextInt(2)+2*nombrePlaque;
        int choiceR = random.nextInt(2)+3*nombrePlaque;
        yellowPlaque = creatPlaque("src/main/resources/boardText/"+listePlaque.get(choiceY));
        greenPlaque = creatPlaque("src/main/resources/boardText/"+listePlaque.get(choiceG));
        bluePlaque = creatPlaque("src/main/resources/boardText/"+listePlaque.get(choiceB));
        redPlaque = creatPlaque("src/main/resources/boardText/"+listePlaque.get(choiceR));

        listePlaquePng.add(listePlaque.get(choiceY));
        listePlaquePng.add(listePlaque.get(choiceG));
        listePlaquePng.add(listePlaque.get(choiceB));
        listePlaquePng.add(listePlaque.get(choiceR));

        int compteurY = 0;
        for (int x = 0; x<length;x++){
            for (int y = 0; y<width;y++ ){
                board[x][y] = yellowPlaque.get(compteurY);
                compteurY++;
            }
        }


        int compteurG = 0;
        for (int x = 0; x<length;x++){
            for (int y = 0; y<width;y++ ){
                board[x][y+width] = greenPlaque.get(compteurG);
                compteurG++;

            }
        }

        int compteurB = 0;
        for (int x = 0; x<length;x++){
            for (int y = 0; y<width;y++ ){
                board[x+length][y] = bluePlaque.get(compteurB);
                compteurB++;

            }
        }

        int compteurR = 0;
        for (int x = 0; x<length;x++){
            for (int y = 0; y<width;y++ ){
                board[x+length][y+width] = redPlaque.get(compteurR);
                compteurR++;

            }
        }

        return board;
    }
}
