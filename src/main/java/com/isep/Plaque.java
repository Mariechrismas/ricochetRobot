package com.isep;

import java.util.ArrayList;
import java.util.List;

import static com.utils.InputParser.readFile;

public class Plaque {
    private List<Case> Plaque = new ArrayList<>();

    /**
     *  creatPlaque, fonction permettant de déclarer les plaques de couleurs
     *  PS : il y a de la consistance dans l'orthographe
     */

    public static List<Case> creatPlaque (String nomPlaque){
        ArrayList<Case> listeCase = new ArrayList<>();
        ArrayList<String> lines = readFile(nomPlaque);

        String separateurCase = ",";
        String[] cases =  lines.get(0).split(separateurCase);

        String[] mots = new String[7];

        String separateur =" ";

        for (int i = 0; i< cases.length;i++){
            Case creatCase = new Case(false,false,false,false,0,0,"0");
            mots = cases[i].split(separateur);
            creatCase.wallNorth = mots[0].equals("1");
            creatCase.wallSouth = mots[1].equals("1");
            creatCase.wallEast = mots[2].equals("1");
            creatCase.wallWest = mots[3].equals("1");
            creatCase.objectif = mots[4];
            listeCase.add(creatCase);
        }

        return listeCase;
    }

}
