package com.isep;

import com.utils.InputParser;
import com.utils.ThreadGraphique;
import com.utils.Timer;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import static com.isep.Board.creatBoard;
import static com.isep.Move.move;
import static com.utils.Timer.numberList;
import static com.utils.Timer.playersList;
import static com.utils.InputParser.*;

public class Game {
    public static List<Player> players= new ArrayList<>();
    public static List<String> listObjectif = new ArrayList<>();
    public static List<Piece> pieceList = new ArrayList<>();
    private static String choiceObjectifString;
    public static String pieceObjectifGraphique="A";
    public static boolean choixGraphique;
    public static AtomicInteger time= new AtomicInteger();
    public static boolean option;
    public static boolean victory = false;
    public static int numberPlayer;
    public static Case[][] board;
    public static int compteurMove = 0;




    /**
     * game(),fonction appelant toutes les autres lancant le jeu
     */
    static void game() throws InterruptedException {
        for (int i=0; i<5; i++){
            pieceList.add(new Piece(0,new Position(0,0),new Position(0,0)));
        }
        board= creatBoard();
        System.out.println("Voulez-vous jouez en 1- graphique ou en 2- Terminal?");
        choixGraphique= InputParser.scannerBool();
        if (choixGraphique){
            ThreadGraphique threadGraphique = new ThreadGraphique();
            threadGraphique.start();
        }
            rules();
            choiceOption();
            creationPlayer();
            displayBoard(board);
            settingGame(option);
            board = synchroPieceBoard(board);
            displayBoard(board);
            listObjectif = extractObjectif(board);
            while (listObjectif.size() != 0){
                int pieceObjectif = choiceObjectif(listObjectif,option);
                choiceGameOver();
                InputParser.dodo(1000);
                playGame(board,pieceObjectif);
            }
        }

    /**
     * displayScore(),fonction permettant d'afficher les scores
     */
    private static void displayScore(){
        for (int compteur = 0; compteur<players.size();compteur++){
            String nom  = players.get(compteur).getName();
            writeLn("Le joueur "+nom+" a un score de "+players.get(compteur).getScore());
        }
    }

    /**
     * rules(),fonction permettant à l'utilisateur de prendre connaissance des règles s'il le souhaite
     */
    public static void rules() throws InterruptedException {
        writeLn("Bienvenu joueur de Ricochet Robot\nAvant de commencer, connaissez vous les règles?");
        System.out.println("Entrez 1 pour Oui et 2 pour non");
        int knowRules= InputParser.scannerInt();
        erreurValeur(knowRules,2,1);
        if (knowRules==2){
            writeLn("Les règles sont trés simples:  \nL’objectif est d’amener le robot qui a la couleur de l’objectif sur la case objectif sur le plateau en effectuant le moins de déplacement. \nPour ce faire, on peut déplacer n’importe lequel des robots. Lorsque l’on avance un robot, il ne peut s’arrêter que lorsqu’il rencontre un obstacle (mur, bord du plateau, plaque centrale, autre robot).\nL’action de déplacer un robot horizontalement ou verticalement compte pour 1, quel que soit le nombre de cases qu’il parcourt.\n" +
                    "Quand on a trouvé une solution, on indique le nombre de déplacement nécessaire. Le sablier démarre pour une durée de 30 secondes. \nTant que le sablier tourne, on peut proposer un autre nombre de déplacement (supérieur ou inférieur). Une fois le temps écoulé,\n" +
                    "le joueur avec le plus petit nombre de déplacements montre sa solution. Si sa solution est correcte,il empoche le jeton Objectif en jeu. Les tuiles Robot sont alors déplacées en-dessous des nouvelles positions des Robots.\n \nMaintenant que vous avez pris connaissance des règles nous allons pouvoir commencer");
            System.out.println("Entrez 1 pour Commencer");
            InputParser.scannerInt();
        }
    }

    /**
     * creationPlayer(),fonction permettant de créer le nombre joueur en fonction du nombre de joueurs voulus.
     */
    private static void creationPlayer() throws InterruptedException {
        numberPlayer = choiceNumberPlayers();
        String name;
        for (int i=0; i<numberPlayer; i++){
                writeLn("Quelle est le nom du joueur J"+i);
                name=InputParser.scannerString();
            players.add(new Player(name,0));
        }
    }

    /**
     * choiceNumberPlayers(),fonction permettant de choisir le nombre de joueurs voulus.
     */
    private static int choiceNumberPlayers() throws InterruptedException {
        int n;
        writeLn("Combien de joueur êtes vous?");
        n = InputParser.scannerInt();
        return n;
    }

    /**
     * erreurValeur(),fonction permettant de vérifier la saisie d'une valeur conforme à la demande, et la
     * redemandent dans le cas où la valeur est non conforme.
     */
    public static void erreurValeur(int valeur, int max, int min) throws InterruptedException {
        while (valeur>max||valeur<min){
            System.out.println("Vous n'avez pas rentré une bonne valeur recommencez:");
            valeur = InputParser.scannerInt();
        }
    }

    /**
     * displayBoard(Case[][] board), fonction permettant d'afficher dans le terminal le plateau de jeu
     */
    public static String[][] displayBoard(Case[][] board) {
        int lengthBoard = board.length;
        int widthBoard = board[0].length;

        String[][] tableDisplayBoard = new String[lengthBoard][widthBoard * 3];

        for (int x = 0; x < lengthBoard; x++) {
            for (int y = 0; y < widthBoard; y++) {
                String[][] caseString = {
                        {"|", "|", "|"},
                        {"-", " ", "-"},
                        {"-", " ", "-"},
                        {"-", " ", "-"},
                        {"|", "|", "|"},
                };

                Case choix = board[y][x];
                if (!Objects.equals(choix.objectif, "0")) {
                    caseString[2][1] = choix.objectif;
                }
                if (choix.pieceJeton != 0) {
                    caseString[1][1] = String.valueOf(choix.pieceJeton);
                }
                if (choix.piece != 0) {
                    caseString[3][1] = String.valueOf(choix.piece);
                }
                if (choix.wallSouth) {
                    caseString[1][2] = "x";
                    caseString[2][2] = "x";
                    caseString[3][2] = "x";
                }
                if (choix.wallNorth) {
                    caseString[1][0] = "x";
                    caseString[2][0] = "x";
                    caseString[3][0] = "x";
                }
                if (choix.wallWest) {
                    caseString[0][0] = "x";
                    caseString[0][1] = "x";
                    caseString[0][2] = "x";
                }
                if (choix.wallEast) {
                    caseString[4][0] = "x";
                    caseString[4][1] = "x";
                    caseString[4][2] = "x";
                }
                tableDisplayBoard[x][y*3] = caseString[0][0]+caseString[1][0]+caseString[2][0]+caseString[3][0]+caseString[4][0];
                tableDisplayBoard[x][y*3+1] = caseString[0][1]+caseString[1][1]+caseString[2][1]+caseString[3][1]+caseString[4][1];
                tableDisplayBoard[x][y*3+2] = caseString[0][2]+caseString[1][2]+caseString[2][2]+caseString[3][2]+caseString[4][2];
            }
        }
        for (int y = 0; y<tableDisplayBoard[0].length;y++){
            System.out.println("");
            for (int x = 0;x<tableDisplayBoard.length; x++){
                System.out.print(tableDisplayBoard[x][y]);
            }
        }
        return tableDisplayBoard;
    }

    /**
     *  settingGame(), fonction permettant d'initier les paramètres des pions [le choix de l'utilisation du pion
     *  noir/argent se fait à partir de la fonction choiceOption] et leurs emplacements
     */
    public static void settingGame(boolean option) throws InterruptedException {
        int numberPiece;
        Random random = new Random();
        String[] arrayColor = {"bleu","rouge","jaune","vert","noir"};

        if (!option){
            numberPiece = arrayColor.length-1;
        }else{
            numberPiece = arrayColor.length;
        }

        int choicePlayer = random.nextInt(players.size());
        System.out.println("\nLe joueur "+players.get(choicePlayer).name+" choisi le placement des pions" );
        for (int i = 0;i<numberPiece ;i++){
            writeLn("Choisisez le placement du pion "+arrayColor[i]+ " en x");
            int positionPieceX =InputParser.scannerInt();
            erreurValeur(positionPieceX, 15,0);
            writeLn("Choisisez le placement du pion "+arrayColor[i]+ " en y");
            int positionPieceY =InputParser.scannerInt();
            erreurValeur(positionPieceY, 15,0);
            Position position = new Position(positionPieceX,positionPieceY);
            Position position1 = new Position(positionPieceX,positionPieceY);
            pieceList.get(i).setPosition(position);
            pieceList.get(i).setPositionInitial(position1);
            pieceList.get(i).setColor(i+1);
        }
    }

    /**
     * synchroPieceBoard(Case[][] board), fonction permettant au plateau de "prendre connaissance" de l'emplacement
     * des autres pions présents sur celui-ci
     */
    public static Case[][] synchroPieceBoard(Case[][] board){
        int lengthBoard = board.length;
        int widthBoard = board[0].length;

        for (int x = 0; x<lengthBoard;x++){
            for (int y = 0; y<widthBoard;y++){
                board[x][y].piece = 0;
                board[x][y].pieceJeton = 0;
            }
        }

        for (int i = 0; i<pieceList.size();i++){
            int col = pieceList.get(i).getPosition().getCol();
            int row = pieceList.get(i).getPosition().getRow();
            board[col][row].piece = pieceList.get(i).getColor();
            int colInitialPosition = pieceList.get(i).getPositionInitial().getCol();
            int rowInitialPosition = pieceList.get(i).getPositionInitial().getRow();
            board[colInitialPosition][rowInitialPosition].pieceJeton = pieceList.get(i).getColor();
        }

        return board;
    }

    /**
     * choiceOption(), fonction permettant de choisir si on joue avec le pion noir/argent
     */
    public static void choiceOption() throws InterruptedException {
        option= false;
        write("Voulez vous joueur avec l'option pion noir");
        System.out.println(": 1= oui 2= non");
        int choiceOption = InputParser.scannerInt();
        System.out.println(choiceOption);
        if (choiceOption == 1){
            option = true;
        }
    }

    /**
     * extractObjectif(Case[][] board), fonction permettant de récuperer les objectifs actuellement sur le plateau
     * (A à Q)
     */
    public static List <String> extractObjectif (Case[][] board){
        int x =board.length;
        int y =board[0].length;

        for (int compteurY = 0 ; compteurY<y; compteurY++){
            for (int compteurX = 0; compteurX<x; compteurX++){
                if (!Objects.equals(board[compteurY][compteurX].objectif, "0")){
                    listObjectif.add(board[compteurY][compteurX].objectif);
                }
            }
        }
        return listObjectif;
    }

    /**
     * choiceObjectif( List<String> listObjectif,boolean option), fonction permettant de choisir aléatoirement un
     * objectif (de A à Q) et de connaître sa couleur
     */
    public static int choiceObjectif( List<String> listObjectif,boolean option){
        int pieceObjectif = 0;
        int lentghArrayColor=4;
        String[][] tabColorPiece = {
                {"A","E","Q","K"},
                {"D","G","O","L"},
                {"C","I","N","M"},
                {"B","H","P","J"},
                {"F","F","F","F"},
        };

        Random random = new Random();
        int choiceObjectif = random.nextInt(listObjectif.size());
        choiceObjectifString = listObjectif.get(choiceObjectif);

        if (option){
            lentghArrayColor = 5;
        }

        for (int compteurY = 0; compteurY<lentghArrayColor; compteurY++){
            for (int compteurX = 0; compteurX<4; compteurX++){
                if (Objects.equals(tabColorPiece[compteurY][compteurX], choiceObjectifString)){
                     pieceObjectif = compteurY+1;
                }
            }
        }

        pieceObjectifGraphique=choiceObjectifString;


        System.out.println("\nVotre Objectif est d'amener le pion "+pieceObjectif+" à "+choiceObjectifString);
        listObjectif.remove(choiceObjectif);
        return pieceObjectif;
    }

    /**
     * playGame(Case[][] board), fonction permettant de lancer le jeu de plateau
     */
    public static void playGame(Case[][] board, int pieceObjectif) throws InterruptedException {
        numberList.clear();
        playersList.clear();

        System.out.println("En combien de coups:");
        numberList.add(InputParser.scannerInt());
        System.out.println("Quel joueur");
        for (int compteur = 0; compteur<players.size();compteur++){
            System.out.println("Taper "+compteur+" pour le joueur: "+players.get(compteur).getName());
        }
        playersList.add(InputParser.scannerInt());
        Timer timer = new Timer();
        timer.start();
        writeLn("Vous avez 30 seconde pour proposer d'autre solution");

        long timeLaunch = System.currentTimeMillis();
        time.set(30);
        while (time.get() !=0  ){
            Thread.sleep(1000);
            time.set(time.get()-1);
        }
        timer.stop();
        writeLn("Temps écoulé");
        moveGlobal(numberList,playersList,board,pieceObjectif);
    }

    /**
     * moveGlobal( List<Integer> numberList, List <Integer> playersList,Case[][]board,int pieceObjectif), fonction
     * permettant de choisir quel mouvement on veut effectuer
     */
    public static void moveGlobal( List<Integer> numberList, List <Integer> playersList,Case[][]board,int pieceObjectif) throws InterruptedException {

        int indice = 0;

        //On clone l'emplacement des pions
        List<Piece> sauvPieceList = new ArrayList<>();
        for (int i = 0; i < pieceList.size(); i++) {
            Position position = new Position(pieceList.get(i).getPosition().row, pieceList.get(i).getPosition().col);
            Position position1 = new Position(pieceList.get(i).getPositionInitial().row, pieceList.get(i).getPositionInitial().col);
            Piece piece = new Piece(pieceList.get(i).getColor(), position, position1);
            sauvPieceList.add(piece);
        }


        // On fait jouer les joueurs
        while (!victory && playersList.size() > 0) {
            //On cherche quel joueur à proposé le plus petit nombre de mouvement
            int numberMoves = numberList.get(0);
            for (int compteur = 0; compteur < numberList.size(); compteur++) {
                if (numberMoves > numberList.get(compteur)) {
                    numberMoves = numberList.get(compteur);
                    indice = compteur;
                }
            }


            //On fait jouer ce joueur
            displayBoard(synchroPieceBoard(board));
            System.out.println("\nLe joueur " + players.get(playersList.get(indice)).getName() + " déplace les pions. Il a " + numberList.get(indice) + " coups.");


            while (compteurMove != numberList.get(indice)) {
                System.out.println(" \nChoisisez un pion à déplacer:");

                if (option) {
                    System.out.println("1 : le pion blue\n2 : le pion red\n3 : le pion yellow\n4 : le pion green\n5 : le pion black");
                }else{
                    System.out.println("1 : le pion blue\n2 : le pion red\n3 : le pion yellow\n4 : le pion green");

                }

                int choicePion = InputParser.scannerInt();
                Piece piece = move(board, pieceList.get(choicePion - 1));
                pieceList.get(choicePion - 1).getPosition().setCol(piece.getPosition().getCol());
                pieceList.get(choicePion - 1).getPosition().setRow(piece.getPosition().getRow());

                if (!choixGraphique) {
                    displayBoard(synchroPieceBoard(board));
                } else {
                    synchroPieceBoard(board);
                }
                compteurMove++;
            }

            //On regarde si l'objectif est remplis
            int lengthBoard = board.length;

            int number = 0;
            for (int compteur = 0; compteur < lengthBoard; compteur++) {
                if (pieceObjectif == pieceList.get(compteur).color) {
                    number = compteur;
                    break;
                }
            }

            if (board[pieceList.get(number).getPosition().getCol()][pieceList.get(number).getPosition().getRow()].objectif == choiceObjectifString) {
                writeLn("\nVictoire du joueur " + players.get(playersList.get(indice)).getName());

                for (int nPiece = 0; nPiece<pieceList.size();nPiece++){
                    pieceList.get(nPiece).setPositionInitial(pieceList.get(nPiece).getPosition());
                }


                for (int compteur = 0; compteur < players.size(); compteur++) {
                    if (players.get(compteur).getName().equals(players.get(playersList.get(indice)).getName())) {
                        players.get(compteur).setScore(players.get(compteur).getScore() + 1);
                    }
                }
                displayScore();
                victory = true;
            } else {
                writeLn("\nEchec l'objectif n'est pas atteint");
                playersList.remove(indice);
                numberList.remove(indice);
                compteurMove = 0;
                for (int i = 0; i < sauvPieceList.size(); i++) {
                    Position position = new Position(sauvPieceList.get(i).getPosition().row, sauvPieceList.get(i).getPosition().col);
                    Position position1 = new Position(sauvPieceList.get(i).getPositionInitial().row, sauvPieceList.get(i).getPositionInitial().col);
                    Piece piece = new Piece(sauvPieceList.get(i).getColor(), position, position1);
                    pieceList.set(i, piece);
                }

            }

        }
    }

    public static void  choiceGameOver() throws InterruptedException {
        int choiceGameOver;
        writeLn("Voulez vous continuer à jouer");
        System.out.println("1 : Oui \n2 : non");
        choiceGameOver = InputParser.scannerInt();
        if (choiceGameOver == 2){
            listObjectif.clear();
        }
    }
}

