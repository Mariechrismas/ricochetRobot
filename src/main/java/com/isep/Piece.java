package com.isep;

public class Piece {
    protected int color;
    protected Position position;
    protected Position positionInitial;

    /**
     *  Piece(int pieceColor, Position piecePosition, Position initialPosition), fonction permettant de définir les
     *  paramètres d'un pion (couleur, position, position initiale) à partir de fonctions préexistantes
     */

    public Piece(int pieceColor, Position piecePosition, Position initialPosition){
        this.color=pieceColor;
        this.position=piecePosition;
        this.positionInitial=initialPosition;
    }

    /**
     *  getColor(), fonction permettant de récupérer la couleur d'un pion
     */

    public int getColor() {
        return color;
    }

    /**
     *  getPosition(), fonction permettant de récupérer la position d'un pion
     */

    public Position getPosition() {
        return position;
    }

    /**
     *  getPositionInitial(), fonction permettant de récupérer la position initiale d'un pion
     */

    public Position getPositionInitial() {
        return positionInitial;
    }

    /**
     *  setPositionInitial(Position positionInitial), fonction permettant de définir la position initiale d'un pion
     */

    public void setPositionInitial(Position positionInitial) {
        this.positionInitial = positionInitial;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
