package com.utils;

import com.example.ricochetrobot.HelloApplication;

public class ThreadGraphique extends Thread{
    public static int valueInteger;
    public static String valueStringIn;
    public static String valueStringOut;

    /**
     *  run(), fonction permettant de lancer l'interface graphique en parallèle de Game.java dans un thread
     */

    public void run() {
        HelloApplication.main();
    }
}
