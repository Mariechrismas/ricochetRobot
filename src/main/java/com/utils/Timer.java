package com.utils;

import java.util.ArrayList;
import java.util.List;


import static com.isep.Game.players;
import static com.utils.InputParser.writeLn;

public class Timer extends Thread {
    public static List<Integer> numberList = new ArrayList<>();
    public static List <Integer> playersList = new ArrayList<>();
    public static Timer timer;

    /**
     *  run(), fonction permettant de gérer les entrées utilisateurs pendant que Game.java est en pause
     */

    public void run() {
        try {
            while (true) {
                Thread.sleep(100);
                writeLn("En combien de coups?");
                numberList.add(InputParser.scannerInt());
                writeLn("Quel joueur ?");
                for (int compteur = 0; compteur<players.size();compteur++){
                    writeLn("Taper "+compteur+" pour le joueur: "+players.get(compteur).getName());
                }
                playersList.add(InputParser.scannerInt());
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
