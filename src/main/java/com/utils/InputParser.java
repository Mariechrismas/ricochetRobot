package com.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

import static com.isep.Game.choixGraphique;

public class InputParser {
    public static int scannerInt() throws InterruptedException {
        ThreadGraphique.valueInteger=999;
        if (choixGraphique){
            while(ThreadGraphique.valueInteger ==999){
                Thread.sleep(1);
            }
        }else{
            Scanner sc=new Scanner(System.in);
            String value = sc.nextLine();
            while (!scannerIntVerification(value))
            {
                System.out.println("Ce n'est pas un nombre veuillez recommencer: ");
                value = sc.nextLine();
            }
            ThreadGraphique.valueInteger = Integer.parseInt(value);
        }
        System.out.println(ThreadGraphique.valueInteger);
        return ThreadGraphique.valueInteger;
    }

    public static boolean scannerBool(){
            Scanner sc=new Scanner(System.in);
            int n=0;
            String value = sc.nextLine();
            while (!scannerIntVerification(value))
            {
                System.out.println("Ce n'est pas un nombre veuillez recommencer: ");
                value = sc.nextLine();
            }
            n = Integer.parseInt(value);
            if (n==1)
                return true;
            else
                return false;
    }

    private static boolean scannerIntVerification(String n){
        try{
            Integer.parseInt(n);
            return true;
        } catch (NumberFormatException e){
            return false;
        }
    }

    public static String scannerString() throws InterruptedException {
        ThreadGraphique.valueStringIn = " ";

        if (choixGraphique){
            while(Objects.equals(ThreadGraphique.valueStringIn, " ")){
                Thread.sleep(1);
            }
        }else {
            Scanner sc = new Scanner(System.in);
            ThreadGraphique.valueStringIn = sc.nextLine();
        }
        System.out.println(ThreadGraphique.valueStringIn);
        return ThreadGraphique.valueStringIn;
    }

    public static void writeLn(String pencil){
        //if (!choixGraphique)
            ThreadGraphique.valueStringOut = pencil;
            System.out.println(pencil);
    }

    public static void write(String pencil){
        ThreadGraphique.valueStringOut = pencil;
        System.out.print(pencil);
    }

    public static String timer(long i, int j) {
        long second =  ((System.currentTimeMillis()-i) / 1000) % 60;
        if (second>=j)
            return "Time out";
        return null;
    }

    public static ArrayList<String> readFile(String name) {
        ArrayList<String> lines = new ArrayList<>();
        BufferedReader reader;
        File file = new File(name);
        try {
            reader = new BufferedReader(new FileReader(file));
            String line = reader.readLine();
            while (line != null) {
                lines.add(line);
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }

    public static void dodo(int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
