package com.example.ricochetrobot;

import com.isep.*;
import com.utils.InputParser;
import com.utils.ThreadGraphique;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

import static com.isep.Game.*;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

import static com.utils.ThreadGraphique.*;

public class HelloApplication extends Application {
    @Override
    public void start(Stage stage) throws InterruptedException{
        Group grid = new Group();

        Button buttonYes = new Button("Oui");
        buttonYes.setLayoutX(0);
        buttonYes.setLayoutY(40);

        Button buttonNo = new Button("Non");
        buttonNo.setLayoutX(40);
        buttonNo.setLayoutY(40);

        Button buttonYes2 = new Button("Oui");
        buttonYes2.setLayoutX(0);
        buttonYes2.setLayoutY(40);
        buttonYes2.setVisible(false);

        Button buttonNo2 = new Button("Non");
        buttonNo2.setLayoutX(40);
        buttonNo2.setLayoutY(40);
        buttonNo2.setVisible(false);

        Button button1 = new Button("1");
        button1.setLayoutX(0);
        button1.setLayoutY(40);
        button1.setVisible(false);

        Button button2 = new Button("2");
        button2.setLayoutX(40);
        button2.setLayoutY(40);
        button2.setVisible(false);

        Button buttonSumbit = new Button("Submit");
        buttonSumbit.setLayoutX(0);
        buttonSumbit.setLayoutY(80);
        buttonSumbit.setVisible(false);

        Button buttonCommencer = new Button("Submit");
        buttonCommencer.setLayoutX(1000);
        buttonCommencer.setLayoutY(300);
        buttonCommencer.setVisible(false);

        //Question regles
        Label pwL = new Label(valueStringOut);
        pwL.setLayoutY(0);
        pwL.setLayoutX(0);
        //Oui je connais les régles

        buttonYes.setOnMouseClicked((event -> {
                ThreadGraphique.valueInteger = 1;
                InputParser.dodo(100);
                pwL.setText(valueStringOut);
                grid.getChildren().removeAll(buttonNo);
                buttonNo2.setVisible(true);
                buttonYes2.setVisible(true);
            }));

        //Non je ne connais pas les régles
        buttonNo.setOnMouseClicked((event -> {
            ThreadGraphique.valueInteger = 2;
            InputParser.dodo(100);
            pwL.setText(valueStringOut);
            buttonYes.setText("Continuer");
            buttonYes.setLayoutY(160);
            grid.getChildren().removeAll(buttonNo);
        }));

        //Choix option pions noir
        AtomicBoolean option = new AtomicBoolean();

        //Non pour le pion noir
        buttonNo2.setOnMouseClicked(event ->{
            ThreadGraphique.valueInteger = 2;
            InputParser.dodo(100);
            pwL.setText(valueStringOut);
            button1.setVisible(true);
            button2.setVisible(true);
            option.set(false);
            grid.getChildren().removeAll(buttonYes, buttonYes2, buttonNo2);
        });

        //Oui pour le pion noir
        buttonYes2.setOnMouseClicked(event ->{
            ThreadGraphique.valueInteger = 1;
            InputParser.dodo(100);
            pwL.setText(valueStringOut);
            button1.setVisible(true);
            button2.setVisible(true);
            grid.getChildren().removeAll(buttonYes, buttonYes2, buttonNo2);
            option.set(true);
        });

        final TextField firstName = new TextField();
        firstName.setPromptText("First player name.");
        firstName.setPrefColumnCount(10);
        firstName.getText();
        firstName.setVisible(false);
        firstName.setLayoutX(0);
        firstName.setLayoutY(20);

        final TextField secondName = new TextField();
        secondName.setPromptText("Second player name.");
        secondName.setPrefColumnCount(10);
        secondName.setVisible(false);
        secondName.setLayoutX(0);
        secondName.setLayoutY(40);

        final TextField position = new TextField();
        position.setVisible(false);

        button1.setOnMouseClicked((event ->{
            ThreadGraphique.valueInteger = 1;
            InputParser.dodo(100);
            pwL.setText(valueStringOut);
            firstName.setVisible(true);
            buttonSumbit.setVisible(true);
            grid.getChildren().removeAll(button1, button2);
        }));

        button2.setOnMouseClicked((envent ->{
            ThreadGraphique.valueInteger = 2;
            InputParser.dodo(100);
            pwL.setText(valueStringOut);
            firstName.setVisible(true);
            secondName.setVisible(true);
            buttonSumbit.setVisible(true);
            grid.getChildren().removeAll(button1, button2);
        }));

        Scene scene = new Scene(grid);
        grid.getChildren().addAll(buttonYes,buttonYes2, button1, button2, buttonNo, buttonNo2, buttonSumbit, buttonCommencer, position, secondName, firstName);

        stage.setTitle("Ricochet Robot");
        stage.setScene(scene);
        scene.setFill(Color.rgb(255, 232, 214));
        stage.setHeight(800);
        stage.setWidth(1400);
        stage.show();

        Image image = new Image(Objects.requireNonNull(getClass().getResource("/boardPictures/"+Board.listePlaquePng.get(0)+".png")).toString(),350,350,false,false);
        ImageView imageView = new ImageView();
        imageView.setImage(image);
        imageView.setVisible(false);

        Image image2 = new Image(Objects.requireNonNull(getClass().getResource("/boardPictures/"+Board.listePlaquePng.get(2)+".png")).toString(),350,350,false,false);
        ImageView imageView2 = new ImageView();
        imageView2.setImage(image2);
        imageView2.setX(0);
        imageView2.setY(350);
        imageView2.setVisible(false);

        Image image3 = new Image(Objects.requireNonNull(getClass().getResource("/boardPictures/"+Board.listePlaquePng.get(1)+".png")).toString(),350,350,false,false);
        ImageView imageView3 = new ImageView();
        imageView3.setImage(image3);
        imageView3.setX(350);
        imageView3.setY(0);
        imageView3.setVisible(false);

        Image image4 = new Image(Objects.requireNonNull(getClass().getResource("/boardPictures/"+Board.listePlaquePng.get(3)+".png")).toString(),350,350,false,false);
        ImageView imageView4 = new ImageView();
        imageView4.setImage(image4);
        imageView4.setX(350);
        imageView4.setY(350);
        imageView4.setVisible(false);

        buttonSumbit.setOnMouseClicked((event ->{
            if ((Objects.equals(firstName.getText(), ""))||(players.size()==2 && Objects.equals(secondName.getText(), ""))){
                pwL.setText("Vous n'avez pas rentré de nom");
            }else {
                pwL.setText(valueStringOut);
                ThreadGraphique.valueStringIn= firstName.getText();
                if (numberPlayer==2){
                    InputParser.dodo(1000);
                    ThreadGraphique.valueStringIn= secondName.getText();
                }
                buttonCommencer.setVisible(true);
                imageView.setVisible(true);
                imageView2.setVisible(true);
                imageView3.setVisible(true);
                imageView4.setVisible(true);
                position.setVisible(true);
                position.setLayoutX(1000);
                position.setLayoutY(250);
                grid.getChildren().removeAll(buttonSumbit, secondName, firstName);
            }
        }));

        Image imageRed = new Image(Objects.requireNonNull(getClass().getResource("/characters/calcifer.png")).toString(),130,200,false,false);
        ImageView imageViewRed = new ImageView();
        Button buttonRed = new Button("", imageViewRed);
        buttonRed.setStyle("-fx-base: #eebe98;");
        buttonRed.setLayoutX(750);
        buttonRed.setLayoutY(0);
        buttonRed.setVisible(false);

        Image imageBlue = new Image(Objects.requireNonNull(getClass().getResource("/characters/monsieur.png")).toString(),130,200,false,false);
        ImageView imageViewBlue = new ImageView();
        Button buttonBlue = new Button("", imageViewBlue);
        buttonBlue.setStyle("-fx-base: #a2a5c7");
        buttonBlue.setLayoutX(1275);
        buttonBlue.setLayoutY(0);
        buttonBlue.setVisible(false);

        Image imageGreen = new Image(Objects.requireNonNull(getClass().getResource("/characters/toroto.png")).toString(),130,200,false,false);
        ImageView imageViewGreen = new ImageView();
        Button buttonGreen = new Button("", imageViewGreen);
        buttonGreen.setStyle("-fx-base: #b7b7a4");
        buttonGreen.setLayoutX(925);
        buttonGreen.setLayoutY(0);
        buttonGreen.setVisible(false);

        Image imageYellow = new Image(Objects.requireNonNull(getClass().getResource("/characters/barront.png")).toString(),130,200,false,false);
        ImageView imageViewYellow = new ImageView();
        Button buttonYellow = new Button("", imageViewYellow);
        buttonYellow.setStyle("-fx-base: #e5d6b0");
        buttonYellow.setLayoutX(1100);
        buttonYellow.setLayoutY(0);
        buttonYellow.setVisible(false);

        Image imageBlack = new Image(Objects.requireNonNull(getClass().getResource("/characters/noface.png")).toString(),130,200,false,false);
        ImageView imageViewBlack = new ImageView();
        imageViewBlack.setImage(imageBlack);
        Button buttonBlack = new Button("", imageViewBlack);
        buttonBlack.setStyle("-fx-base: #939194");
        buttonBlack.setLayoutX(1450);
        buttonBlack.setLayoutY(0);
        buttonBlack.setVisible(false);

        Image imageUp = new Image(Objects.requireNonNull(getClass().getResource("/positions/up.png")).toString(),100,100,false,false);
        ImageView imageViewUp = new ImageView();
        imageViewUp.setImage(imageUp);
        Button buttonUp = new Button("", imageViewUp);
        buttonUp.setStyle("-fx-base: #ddbea9;");
        buttonUp.setLayoutX(835);
        buttonUp.setLayoutY(300);
        buttonUp.setVisible(false);

        Image imageLeft = new Image(Objects.requireNonNull(getClass().getResource("/positions/left.png")).toString(),100,100,false,false);
        ImageView imageViewLeft = new ImageView();
        imageViewLeft.setImage(imageLeft);
        Button buttonLeft = new Button("", imageViewLeft);
        buttonLeft.setStyle("-fx-base: #ddbea9;");
        buttonLeft.setLayoutX(715);
        buttonLeft.setLayoutY(425);
        buttonLeft.setVisible(false);

        Image imageRight = new Image(Objects.requireNonNull(getClass().getResource("/positions/right.png")).toString(),100,100,false,false);
        ImageView imageViewRight = new ImageView();
        imageViewRight.setImage(imageRight);
        Button buttonRight = new Button("", imageViewRight);
        buttonRight.setStyle("-fx-base: #ddbea9");
        buttonRight.setLayoutX(965);
        buttonRight.setLayoutY(425);
        buttonRight.setVisible(false);

        Image imageDown = new Image(Objects.requireNonNull(getClass().getResource("/positions/down.png")).toString(),100,100,false,false);
        ImageView imageViewDown = new ImageView();
        imageViewDown.setImage(imageDown);
        Button buttonDown = new Button("", imageViewDown);
        buttonDown.setStyle("-fx-base: #ddbea9;");
        buttonDown.setLayoutX(835);
        buttonDown.setLayoutY(550);
        buttonDown.setVisible(false);

        ImageView imageViewScore = new ImageView();
        imageViewScore.setX(1120);
        imageViewScore.setY(300);
        imageViewScore.setVisible(false);

        final TextField numberMoveJ1 = new TextField();
        numberMoveJ1.setVisible(false);
        numberMoveJ1.setPrefColumnCount(10);
        numberMoveJ1.getText();
        numberMoveJ1.setLayoutX(1120);
        numberMoveJ1.setLayoutY(400);
        numberMoveJ1.setVisible(false);

        Button submit = new Button("Sousmettre");
        submit.setLayoutX(1120);
        submit.setLayoutY(460);
        submit.setVisible(false);

        Button continuer = new Button("Continuer");
        continuer.setLayoutX(1120);
        continuer.setLayoutY(460);
        continuer.setVisible(false);

        Button annuler = new Button("Annuler");
        annuler.setLayoutX(1120);
        annuler.setLayoutY(490);
        annuler.setVisible(false);

        Button stop = new Button("Arrêter");
        stop.setVisible(false);
        stop.setLayoutX(1220);
        stop.setLayoutY(460);

        final TextField numberMoveJ2 = new TextField();
        numberMoveJ2.setVisible(false);
        Label scoreJ2Label = new Label();

        Circle circleRed = new Circle();
        circleRed.setRadius(15.0f);
        circleRed.setFill(Color.rgb(231, 111, 81));
        circleRed.setVisible(false);

        Circle circleYellow = new Circle();
        circleYellow.setRadius(15.0f);
        circleYellow.setFill(Color.rgb(233, 196, 106));
        circleYellow.setVisible(false);

        Circle circleBlue = new Circle();
        circleBlue.setRadius(15.0f);
        circleBlue.setFill(Color.rgb(74, 78, 105));
        circleBlue.setVisible(false);

        Circle circleGreen = new Circle();
        circleGreen.setRadius(15.0f);
        circleGreen.setFill(Color.rgb(165, 165, 141));
        circleGreen.setVisible(false);

        Circle circleBlack = new Circle();
        circleBlack.setRadius(15.0f);
        circleBlack.setFill(Color.rgb(34, 34, 59));
        circleBlack.setVisible(false);

        Label scoreJ1Label = new Label();

        AtomicInteger nbClick = new AtomicInteger();
        nbClick.set(0);
        buttonCommencer.setOnMouseClicked((event ->{
            InputParser.dodo(100);
            pwL.setText(valueStringOut);
            pwL.setLayoutY(200);
            pwL.setLayoutX(1000);
            ThreadGraphique.valueInteger= Integer.parseInt((position.getText()));
            nbClick.set(nbClick.get()+1);
            if (nbClick.get()==2*pieceList.size()){
                InputParser.dodo(100);
                ThreadGraphique.valueInteger = 1;
                pwL.setLayoutX(1120);
                pwL.setLayoutY(550);
                stop.setVisible(true);
                buttonCommencer.setVisible(false);
                position.setVisible(false);
                scoreJ1Label.setText("Score "+ players.get(0).getName() +": "+players.get(0).getScore());
                scoreJ1Label.setLayoutX(1270);
                scoreJ1Label.setLayoutY(405);
                buttonCommencer.setText("Commencer");
                buttonBlue.setVisible(true);
                buttonGreen.setVisible(true);
                buttonRed.setVisible(true);
                buttonYellow.setVisible(true);
                if (option.get()) {
                    buttonBlack.setVisible(true);
                    circleBlack.setVisible(true);
                }
                buttonUp.setVisible(true);
                buttonLeft.setVisible(true);
                buttonRight.setVisible(true);
                buttonDown.setVisible(true);
                numberMoveJ1.setVisible(true);
                submit.setVisible(true);
                annuler.setVisible(true);
                imageViewScore.setVisible(true);
                InputParser.dodo(1000);
                imageViewScore.setImage(new Image(Objects.requireNonNull(getClass().getResource("/score/"+ pieceObjectifGraphique+".png")).toString(),100,100,false,false));
                scoreJ1Label.setVisible(true);
                circleBlue.setVisible(true);
                circleRed.setVisible(true);
                circleGreen.setVisible(true);
                circleYellow.setVisible(true);

                imageViewRed.setImage(imageRed);
                imageViewBlue.setImage(imageBlue);
                imageViewGreen.setImage(imageGreen);
                imageViewYellow.setImage(imageYellow);

                numberMoveJ1.setPromptText("Nombre de coups "+players.get(0).getName());

                if (players.size()==2){
                    numberMoveJ2.setPrefColumnCount(10);
                    numberMoveJ2.getText();
                    numberMoveJ2.setLayoutX(1120);
                    numberMoveJ2.setLayoutY(425);
                    scoreJ2Label.setText("Score "+players.get(1).getName()+": "+players.get(1).getScore());
                    scoreJ2Label.setLayoutX(1270);
                    scoreJ2Label.setLayoutY(430);
                    numberMoveJ2.setVisible(true);
                    scoreJ2Label.setVisible(true);
                    numberMoveJ2.setPromptText("Nombre de coups "+ players.get(1).getName());
                }
                updatePosition(circleRed, circleYellow, circleBlue, circleGreen, circleBlack);
            }
        }));

        Label compteur = new Label();
        compteur.setVisible(false);
        compteur.setLayoutX(1120);
        compteur.setLayoutY(570);

        AtomicInteger nbClick2 = new AtomicInteger();
        submit.setOnMouseClicked(event -> {
            compteur.setVisible(true);
                pwL.setText(valueStringOut);
                if (!Objects.equals(numberMoveJ1.getText(), "") && Objects.equals(numberMoveJ2.getText(), "") && nbClick2.get()==0){
                    ThreadGraphique.valueInteger= Integer.parseInt(numberMoveJ1.getText());
                    InputParser.dodo(1000);
                    ThreadGraphique.valueInteger=0;
                }else if (Objects.equals(numberMoveJ1.getText(), "") && !Objects.equals(numberMoveJ2.getText(), "") && nbClick2.get()==0){
                    ThreadGraphique.valueInteger= Integer.parseInt(numberMoveJ2.getText());
                    InputParser.dodo(1000);
                    ThreadGraphique.valueInteger=1;
                }else if (!Objects.equals(numberMoveJ1.getText(), "") && !Objects.equals(numberMoveJ2.getText(), "") && nbClick2.get()==0) {
                    int numberJ1 = Integer.parseInt(numberMoveJ1.getText());
                    int numberJ2 = Integer.parseInt(numberMoveJ2.getText());
                    if (numberJ1 < numberJ2) {
                        ThreadGraphique.valueInteger = Integer.parseInt(numberMoveJ1.getText());
                        InputParser.dodo(1000);
                        ThreadGraphique.valueInteger = 0;
                    } else {
                        ThreadGraphique.valueInteger = Integer.parseInt(numberMoveJ2.getText());
                        InputParser.dodo(1000);
                        ThreadGraphique.valueInteger = 1;
                    }
                }
            submit.setVisible(false);
            continuer.setVisible(true);
        });

        continuer.setOnMouseClicked(event ->{
            InputParser.dodo(1000);
            imageViewScore.setImage(new Image(Objects.requireNonNull(getClass().getResource("/score/"+ pieceObjectifGraphique+".png")).toString(),100,100,false,false));
            ThreadGraphique.valueInteger=1;
            submit.setText("Submit");
            numberMoveJ1.clear();
            numberMoveJ2.clear();
            submit.setVisible(true);
            continuer.setVisible(false);
        });

        stop.setOnMouseClicked(event ->{
            ThreadGraphique.valueInteger=2;
            stage.close();
        });

        annuler.setOnMouseClicked(event -> {
            compteurMove=0;
            for (Piece piece : pieceList) {
                Position position2 = new Position(piece.getPositionInitial().getRow(), piece.getPositionInitial().getRow());
                piece.setPosition(position2);
            }
            updatePosition(circleRed, circleYellow, circleBlue, circleGreen, circleBlack);
        });

            buttonBlue.setOnMouseClicked(event -> ThreadGraphique.valueInteger=1);
            buttonGreen.setOnMouseClicked(event -> ThreadGraphique.valueInteger=4);
            buttonRed.setOnMouseClicked(event -> ThreadGraphique.valueInteger=2);
            buttonYellow.setOnMouseClicked(event -> ThreadGraphique.valueInteger=3);
            buttonBlack.setOnMouseClicked(event -> ThreadGraphique.valueInteger=4);

            buttonDown.setOnMouseClicked(event -> {
                compteur.setText("Nombre de nouvements: "+ (compteurMove + 1));
                ThreadGraphique.valueInteger=2;
                InputParser.dodo(100);
                updatePosition(circleRed, circleYellow, circleBlue, circleGreen, circleBlack);
                InputParser.dodo(100);
                if(victory){
                    scoreJ1Label.setText("Score "+ players.get(0).getName() +": "+players.get(0).getScore());
                    if (players.size()==2){
                        scoreJ2Label.setText("Score "+ players.get(1).getName() +": "+players.get(1).getScore());
                    }
                    pwL.setText(ThreadGraphique.valueStringOut);
                }
            });
            buttonLeft.setOnMouseClicked(event -> {
                compteur.setText("Nombre de nouvements: "+ (compteurMove+1));
                ThreadGraphique.valueInteger=3;
                InputParser.dodo(100);
                updatePosition(circleRed, circleYellow, circleBlue, circleGreen, circleBlack);
                if(victory){
                    scoreJ1Label.setText("Score "+ players.get(0).getName() +": "+players.get(0).getScore());
                    if (players.size()==2){
                        scoreJ2Label.setText("Score "+ players.get(1).getName() +": "+players.get(1).getScore());
                    }
                    pwL.setText(ThreadGraphique.valueStringOut);
                }
            });
            buttonRight.setOnMouseClicked(event -> {
                compteur.setText("Nombre de nouvements: "+ (compteurMove+1));
                ThreadGraphique.valueInteger=4;
                InputParser.dodo(100);
                updatePosition( circleRed, circleYellow, circleBlue, circleGreen, circleBlack);
                if(victory){
                    scoreJ1Label.setText("Score "+ players.get(0).getName() +": "+players.get(0).getScore());
                    if (players.size()==2){
                        scoreJ2Label.setText("Score "+ players.get(1).getName() +": "+players.get(1).getScore());
                    }
                    pwL.setText(ThreadGraphique.valueStringOut);
                }
            });

            buttonUp.setOnMouseClicked(event -> {
                compteur.setText("Nombre de nouvements: "+ (compteurMove+1));
                ThreadGraphique.valueInteger=1;
                InputParser.dodo(100);
                updatePosition(circleRed, circleYellow, circleBlue, circleGreen, circleBlack);
                if(victory){
                    scoreJ1Label.setText("Score "+ players.get(0).getName() +": "+players.get(0).getScore());
                    if (players.size()==2){
                        scoreJ2Label.setText("Score "+ players.get(1).getName() +": "+players.get(1).getScore());
                    }
                    pwL.setText(ThreadGraphique.valueStringOut);
                }
            });

        updatePosition(circleRed, circleYellow, circleBlue, circleGreen, circleBlack);
        playMusic();
        grid.getChildren().addAll(continuer, stop, compteur, pwL, imageView, imageView2, imageView3, imageView4, buttonBlue,buttonGreen,buttonRed,buttonYellow,buttonUp,buttonLeft,buttonRight,buttonDown, numberMoveJ1, numberMoveJ2, annuler, submit, imageViewScore, scoreJ1Label, scoreJ2Label,circleRed, circleYellow, circleBlue, circleGreen, buttonBlack, circleBlack, imageViewBlack);
    }
    public static void main() {
        launch();
    }

    public static void updatePosition( Circle circleRed, Circle circleYellow, Circle circleBlue, Circle circleGreen, Circle circleBlack){
        circleRed.setCenterX(35+42* pieceList.get(1).getPosition().getRow());
        circleRed.setCenterY(35+42* pieceList.get(1).getPosition().getCol());
        circleYellow.setCenterX(35+42* pieceList.get(2).getPosition().getRow());
        circleYellow.setCenterY(35+42* pieceList.get(2).getPosition().getCol());
        circleBlue.setCenterX(35+42* pieceList.get(0).getPosition().getRow());
        circleBlue.setCenterY(35+42* pieceList.get(0).getPosition().getCol());
        circleGreen.setCenterX(35+42* pieceList.get(3).getPosition().getRow());
        circleGreen.setCenterY(35+42* pieceList.get(3).getPosition().getCol());
        circleBlack.setCenterX(35+42* pieceList.get(4).getPosition().getRow());
        circleBlack.setCenterY(35+42* pieceList.get(4).getPosition().getCol());
    }
    public void playMusic() {
        try {
            Clip clip = AudioSystem.getClip();
            InputStream audioSrc = Objects.requireNonNull(getClass().getResourceAsStream("/sound/music.wav"));
            InputStream bufferedIn = new BufferedInputStream(audioSrc);
            AudioInputStream audioStream = AudioSystem.getAudioInputStream(bufferedIn);
            clip.open(audioStream);
            clip.loop(Clip.LOOP_CONTINUOUSLY);
            clip.start();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}